#
# Copyright (C) 2022 The Project Nyanpasu
#           (C) 2022-2023 Project Mia
#
# SPDX-License-Identifier: Apache-2.0
#

# Set GMS client ID base
ifeq ($(PRODUCT_GMS_CLIENTID_BASE),)
PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
    ro.com.google.clientidbase=android-google
else
PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
    ro.com.google.clientidbase=$(PRODUCT_GMS_CLIENTID_BASE)
endif

# Override undesired Google defaults
PRODUCT_PRODUCT_PROPERTIES += \
    keyguard.no_require_sim=true \
    ro.url.legal=http://www.google.com/intl/%s/mobile/android/basic/phone-legal.html \
    ro.url.legal.android_privacy=http://www.google.com/intl/%s/mobile/android/basic/privacy.html

# Add acsa property for CarrierServices
PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.acsa=true

# Google Play services configuration
PRODUCT_PRODUCT_PROPERTIES += \
    ro.error.receiver.system.apps=com.google.android.gms \
    ro.atrace.core.services=com.google.android.gms,com.google.android.gms.ui,com.google.android.gms.persistent

# OPA configuration
PRODUCT_PRODUCT_PROPERTIES += \
    ro.opa.eligible_device=true

# SetupWizard configuration
PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.gmsversion=13_202208 \
    setupwizard.feature.baseline_setupwizard_enabled=true \
    ro.setupwizard.enterprise_mode=1 \
    ro.setupwizard.rotation_locked=true \
    setupwizard.enable_assist_gesture_training=true \
    setupwizard.theme=glif_v3_light \
    setupwizard.feature.skip_button_use_mobile_data.carrier1839=true \
    setupwizard.feature.show_pai_screen_in_main_flow.carrier1839=false \
    setupwizard.feature.show_pixel_tos=false \
    setupwizard.feature.show_support_link_in_deferred_setup=false \
    setupwizard.feature.day_night_mode_enabled=true \
    setupwizard.feature.portal_notification=true

# Android Build system uncompresses DEX files (classes*.dex) in the privileged
# apps by default, which breaks APK v2/v3 signature. Because some privileged
# GMS apps are still shipped with compressed DEX, we need to disable
# uncompression feature to make them work correctly.
DONT_UNCOMPRESS_PRIV_APPS_DEXS := true

PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/app/GoogleExtShared/GoogleExtShared.apk \
    system/app/GoogleExtShared/oat/% \
    system/priv-app/GooglePackageInstaller/GooglePackageInstaller.apk \
    system/priv-app/GooglePackageInstaller/oat/% \
    system/app/GooglePrintRecommendationService/GooglePrintRecommendationService.apk \
    system/app/GooglePrintRecommendationService/oat/% \
    system/etc/permissions/privapp-permissions-google-system.xml \
    system/etc/sysconfig/google-hiddenapi-package-allowlist.xml

# Permission
# System
PRODUCT_PACKAGES += \
    privapp-permissions-google-system.xml \
    google-hiddenapi-package-allowlist.xml

# Product
PRODUCT_PACKAGES += \
    default-permissions-google.xml \
    com.google.android.dialer.support.xml \
    privapp-permissions-google-comms-suite.xml \
    privapp-permissions-google-product.xml \
    split-permissions-google.xml \
    google.xml \
    play_store_fsi_cert.der \
    gms_fsverity_cert.der \
    game_service.xml \
    google_allowlist.xml \
    pixel_2016_exclusive.xml \
    wellbeing.xml

# System_ext
PRODUCT_PACKAGES += \
    privapp-permissions-google-system-ext.xml

# Gapps Packages
PRODUCT_PACKAGES += \
    com.google.android.dialer.support \
    AndroidAutoStub \
    AndroidPlatformServices \
    CalculatorGoogle \
    CalendarGoogle \
    CarrierServices \
    Chrome \
    ConfigUpdater \
    DeskClockGoogle \
    DevicePersonalizationPrebuiltPixel2022 \
    DeviceIntelligenceNetworkPrebuilt \
    GCS \
    GmsCore \
    GoogleCalendarSyncAdapter \
    GoogleContacts \
    GoogleContactsSyncAdapter \
    GoogleDialer \
    GoogleExtShared \
    GoogleFeedback \
    GoogleLocationHistory \
    GoogleOneTimeInitializer \
    GooglePrintRecommendationService \
    GooglePackageInstaller \
    GooglePartnerSetup \
    GoogleRestore \
    GoogleServicesFramework \
    GoogleTTS \
    LatinImeGoogle \
    Messages \
    Phonesky \
    Photos \
    PixelSetupWizard \
    SettingsIntelligenceGooglePrebuilt \
    SetupWizard \
    talkback \
    TrichromeLibrary \
    Velvet \
    WebViewGoogle \
    Wellbeing

# Gapps overlay
PRODUCT_PACKAGES += \
    GmsConfigAiAiOverlay \
    GmsConfigOverlay \
    GmsSettingsOverlay \
    GmsSettingsProviderOverlay \
    PixelSimpleDeviceConfigOverlay \
    PixelSetupWizardOverlay \
    PixelSetupWizardStringsOverlay \
    TelecommConfigOverlay
